#include "../include/State_Game.hpp"
#include "../include/StateManager.hpp"
#include "../include/Utilities.hpp"
#include <algorithm>


State_Game::State_Game(StateManager* l_stateManager)
	: BaseState(l_stateManager)
{
}

State_Game::~State_Game()
{
}

void State_Game::OnCreate()
{
    /*
	m_texture.loadFromFile(Utils::GetWorkingDirectory() + "graphics/Mushroom.png");
	m_sprite.setTexture(m_texture);
	m_sprite.setPosition(0,0);
	m_increment = sf::Vector2f(400.0f,400.0f);
	*/

    EventManager* evMgr = m_stateMgr->GetContext()->m_eventManager;
	evMgr->AddCallback(StateType::Game,"Key_Escape",&State_Game::MainMenu,this);
	evMgr->AddCallback(StateType::Game,"Key_P",&State_Game::Pause,this);
	evMgr->AddCallback(StateType::Game,"Key_O",&State_Game::ToggleOverlay,this);

	// sf::Vector2u size = m_stateMgr->GetContext()->m_wind->GetWindowSize();
	// m_view.setSize(size.x,size.y); // se comenta por redundante son los mismos que la default obtenida en StateManager::CreateState
	// m_view.setCenter(size.x/2,size.y/2); // se comenta por redundante idem arriba 
	m_view.zoom(0.6f); // 480,360 (origen 800,600)
	//m_stateMgr->GetContext()->m_wind->GetRenderWindow()->setView(m_view); // se quita por redundante, se hace posteriromente en StateManager::SwitchTo después de activate

	m_gameMap = new Map(m_stateMgr->GetContext(), this);
	m_gameMap->LoadMap("media/Maps/map1.map"); // ToDo better
}

void State_Game::OnDestroy()
{
    EventManager* evMgr = m_stateMgr->GetContext()->m_eventManager;
	evMgr->RemoveCallback(StateType::Game,"Key_Escape");
	evMgr->RemoveCallback(StateType::Game,"Key_P");
	evMgr->RemoveCallback(StateType::Game, "Key_O");
	
	delete m_gameMap;
	m_gameMap = nullptr;
}

void State_Game::Activate()
{
}

void State_Game::Deactivate()
{
}

void State_Game::Update(const sf::Time& l_time)
{
    /*
	sf::Vector2u l_windSize = m_stateMgr->GetContext()->m_wind->GetWindowSize();
	sf::Vector2u l_textSize = m_texture.getSize();

	if((m_sprite.getPosition().x > l_windSize.x - l_textSize.x && m_increment.x > 0) ||
		(m_sprite.getPosition().x < 0 && m_increment.x < 0))
	{
			m_increment.x = -m_increment.x;
	}

	if((m_sprite.getPosition().y > l_windSize.y - l_textSize.y && m_increment.y > 0) ||
		(m_sprite.getPosition().y < 0 && m_increment.y < 0))
	{
			m_increment.y = -m_increment.y;
	}

	m_sprite.setPosition(m_sprite.getPosition().x + (m_increment.x * l_time.asSeconds()), 
		m_sprite.getPosition().y + (m_increment.y * l_time.asSeconds()));
	*/
// ? no know EntityBase
	SharedContext* context = m_stateMgr->GetContext();
	EntityBase* player = context->m_entityManager->Find("Player");
	// * Player is Alive?
	if(!player){
		std::cout << "Respawning player..." << std::endl;
		context->m_entityManager->Add(EntityType::Player,"Player");
		player = context->m_entityManager->Find("Player");
		player->SetPosition(m_gameMap->GetPlayerStart());
	} else { // * Player exist
		m_view.setCenter(player->GetPosition()); // start center (0,512) size 480,360
		context->m_wind->GetRenderWindow()->setView(m_view);
	}
	// * SCROLLING
	sf::FloatRect viewSpace = context->m_wind->GetViewSpace();
	if(viewSpace.left <= 0){
		m_view.setCenter(viewSpace.width / 2,m_view.getCenter().y); // 240,512
		context->m_wind->GetRenderWindow()->setView(m_view);
	} else if (viewSpace.left + viewSpace.width > (m_gameMap->GetMapSize().x + 1) * Sheet::Tile_Size){
		m_view.setCenter(((m_gameMap->GetMapSize().x + 1) * Sheet::Tile_Size) - (viewSpace.width / 2), m_view.getCenter().y);
		context->m_wind->GetRenderWindow()->setView(m_view);
	}
	/*
	 // * Debug 
    if (m_stateMgr->GetContext()->m_debugOverlay.Debug())
        {
        	sf::RectangleShape* rect = new sf::RectangleShape(m_view.)
			sf::Vector2f tempPos(itr.m_tileBounds.left, itr.m_tileBounds.top);
            sf::RectangleShape* rect = new sf::RectangleShape(sf::Vector2f(tileSize, tileSize));
            rect->setPosition(tempPos);
            rect->setFillColor(sf::Color(255,255,0,150));
            m_entityManager->GetContext()->m_debugOverlay.Add(rect);
        }
    // * End Debug.
	*/


	m_gameMap->Update(std::min(l_time.asSeconds(),0.1f));
	m_stateMgr->GetContext()->m_entityManager->Update(std::min(l_time.asSeconds(),0.1f));
}

void State_Game::Draw(){
	//m_stateMgr->GetContext()->m_wind->GetRenderWindow()->draw(m_sprite);
	m_gameMap->Draw();
	m_stateMgr->GetContext()->m_entityManager->Draw();
}

void State_Game::MainMenu(EventDetails* ){
	m_stateMgr->SwitchTo(StateType::MainMenu); 
}

void State_Game::Pause(EventDetails* ){
	m_stateMgr->SwitchTo(StateType::Paused); 
}

void State_Game::ToggleOverlay(EventDetails* )
{
	m_stateMgr->GetContext()->m_debugOverlay.SetDebug(!m_stateMgr->GetContext()->m_debugOverlay.Debug());
}
