#include "../include/Game.hpp"
#include <algorithm>

Game::Game(): m_window("Plataformas",sf::Vector2u(800,600)), m_entityManager(&m_context, 100), 
m_stateManager(&m_context) {
	m_clock.restart();
	srand(time(nullptr));

	m_context.m_wind = &m_window;
	m_context.m_eventManager = m_window.GetEventManager();
	m_context.m_textureManager = &m_textureManager;
	m_context.m_entityManager = &m_entityManager;
	
	m_stateManager.SwitchTo(StateType::Intro);

	/*
    m_texture.loadFromFile("graphics/Mushroom.png");
    m_sprite.setTexture(m_texture);
    m_sprite.setOrigin(m_texture.getSize().x / 2, m_texture.getSize().y / 2);
    m_sprite.setPosition(0,0);

    m_window.GetEventManager()->AddCallback("Move", &Game::MoveSprite,this);

    // Creando un evento de pulsar la tecla B
    Binding* bind1 = new Binding("BUTTOM_B");
    EventType evT = EventType::KeyDown;
    EventInfo evI(1); //     codigo de la tecla B en enum Key (línea 48) de Keyboard.hpp
	bind1->BindEvent(evT, evI);

    if (! m_window.GetEventManager()->AddBinding(bind1)) {
		delete bind1;
	}
	bind1 = nullptr;
    m_window.GetEventManager()->AddCallback("BUTTOM_B",&Game::Lurch,this);
	// se pone this porque estoy en la implementación de la clase Game. La función está
	// en este mismo cpp más abajo.
	// BUTTOM_A y BUTTON_XY están en el fichero keys.cfg, igual que la linea anterior
	// las funciones implementadas están más abajo.
	m_window.GetEventManager()->AddCallback("BUTTOM_A",&Game::Swap,this);
	m_window.GetEventManager()->AddCallback("BUTTOM_XY",&Game::ComboXY,this);
	*/
}

Game::~Game(){}

sf::Time Game::GetElapsed() { return m_clock.getElapsedTime(); }

void Game::RestartClock() { m_elapsed = m_clock.restart(); }

Window* Game::GetWindow() { return &m_window; }

void Game::Update(){
	m_window.Update();
	m_stateManager.Update(m_elapsed);
}

void Game::Render(){
	m_window.BeginDraw();
	// Render here.
	m_stateManager.Draw();
	//m_window.GetRenderWindow()->draw(m_sprite);

	// Debug.
	if(m_context.m_debugOverlay.Debug()){
		m_context.m_debugOverlay.Draw(m_window.GetRenderWindow());
	}
	// End debug.
	
	m_window.EndDraw();
}

void Game::LateUpdate()
{
	m_stateManager.ProcessRequests();
	RestartClock();
}

/*
void Game::MoveSprite(EventDetails* l_details){
	sf::Vector2i mousepos = m_window.GetEventManager()->GetMousePos(m_window.GetRenderWindow());
	m_sprite.setPosition(mousepos.x, mousepos.y);
	std::cout << "Moving sprite to: " << mousepos.x << ":" << mousepos.y << std::endl;
}

void Game::Lurch (EventDetails* l_details) {
	std::cout << "LURCH COMMAND" << std::endl;
}

void Game::Swap(EventDetails* l_details) {
	std::cout << "SWAP COMMAND" << std::endl;
}

void Game::ComboXY(EventDetails* ldetails) {
	std::cout << "X Y Simultaneas" << std::endl;
}
*/