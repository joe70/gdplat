#include "../include/Game.hpp"

int main(int , char**){
	// Program entry point.
	Game game;
	while(!game.GetWindow()->IsDone()){
		game.Update();
		game.Render();
		game.LateUpdate();
	}
}