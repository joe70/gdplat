#include "../include/StateManager.hpp"

StateManager::StateManager(SharedContext* l_shared)
	: m_shared(l_shared)
{
	RegisterState<State_Intro>(StateType::Intro);
	RegisterState<State_MainMenu>(StateType::MainMenu);
	RegisterState<State_Game>(StateType::Game);
	RegisterState<State_Paused>(StateType::Paused);
	RegisterState<State_GameOver>(StateType::GameOver);
	//RegisterState<State_Credits>(StateType::Credits);
}

StateManager::~StateManager() {
	// TODO shared_ptr no need
	for (auto &itr : m_states){
		itr.second->OnDestroy();
		delete itr.second;
		itr.second=nullptr;
	}
}

void StateManager::Update(const sf::Time& l_time)
{
	if (m_states.empty()){ return; }
	if (m_states.back().second->IsTranscendent() && m_states.size() > 1) {
		// TODO Test reverse iterator
		/* con reverse iterator
		for (auto itr=m_states.rbegin(); itr!=m_states.rend(); ++itr) {
			if (!itr->second->IsTranscendent())
				break;
		}
		*/
		auto itr = m_states.end();
		while (itr != m_states.begin()){
			if (itr != m_states.end()){
				if (!itr->second->IsTranscendent()){
					break;
				}
			}
			--itr;
		}
		// el for comienza donde esté el iterador
		for (; itr != m_states.end(); ++itr){
			itr->second->Update(l_time);
		}
	} else {
		m_states.back().second->Update(l_time);
	} 
}

void StateManager::Draw()
{
    if (m_states.empty()){ return; }
	
	if (m_states.back().second->IsTransparent() && m_states.size() > 1) {
		/*  con reverse iterator - test
		for (auto itr=m_states.rbegin(); itr!=m_states.rend(); ++itr) {
			if (!itr->second->IsTransparent())
				break;
		}
		*/
		auto itr = m_states.end();
		while (itr != m_states.begin()) {
			if (itr != m_states.end()){
				if (!itr->second->IsTransparent()){
					break; // sale del while
				}
			}
			--itr;
		} 
		
		// comienza el for desde donde está el itr, que es el primer estado no transparente
		// termina en el transparente
		for (; itr != m_states.end(); ++itr){
			m_shared->m_wind->GetRenderWindow()->setView(itr->second->GetView());
			itr->second->Draw();
		}
	} 
    else { // si sólo hay un estado o el ultimo estado en el vector no es transparente
		m_states.back().second->Draw();
	}
	// Siempre dibuja el primer no transparente, a partir de ahí dibuja los transparentes si los hay
}

void StateManager::ProcessRequests()
{
	while (m_toRemove.begin() != m_toRemove.end())	{
		RemoveState(*m_toRemove.begin());
		// remove the elements at iterator position and return the position of the next element
		// no necesita incremento, borra la posición actual y retorna la posición del siguiente elemento
		m_toRemove.erase(m_toRemove.begin());
	}
}

SharedContext* StateManager::GetContext()
{
    return m_shared;
}

bool StateManager::HasState(const StateType& l_type)
{ // si no encuentra el estado o si esta está para ser borrado
    for (auto itr = m_states.begin(); itr!= m_states.end() ; ++itr) {
        if (itr->first==l_type) { // estado existe
            auto removed =std::find(m_toRemove.begin(), m_toRemove.end(), l_type);
            if (removed == m_toRemove.end())  // no está para borrado 
                return true; // lo tiene
        }   
		return false;
    }
    return false;
}

void StateManager::Remove(const StateType& l_type)
{
	m_toRemove.push_back(l_type);
}

void StateManager::SwitchTo(const StateType& l_type)
{
	m_shared->m_eventManager->SetCurrentState(l_type);
	// "Stack" States
	for (auto itr = m_states.begin(); itr != m_states.end(); ++itr)
	{
		if (itr->first == l_type)	{
			m_states.back().second->Deactivate();
			StateType tmp_type = itr->first;
			// ¿Copy Constructor? No, it's a memory direction, is a pointer
			BaseState* tmp_state = itr->second;
			m_states.erase(itr);
			m_states.emplace_back(tmp_type, tmp_state);
			tmp_state->Activate();
			// cambiar el espacio de vista de la ventana para que coincida 
			// con el estado al que estamos cambiando
			m_shared->m_wind->GetRenderWindow()->setView(tmp_state->GetView());
			return;
		}
	}
	
	// State with l_type wasn't found.
	if (!m_states.empty()) { 
		m_states.back().second->Deactivate(); 
	}
	CreateState(l_type);
	m_states.back().second->Activate();
	// cambiar el espacio de vista de la ventana para que coincida 
	// con el estado al que estamos cambiando
	m_shared->m_wind->GetRenderWindow()->setView(m_states.back().second->GetView());
}

void StateManager::CreateState(const StateType& l_type)
{
	auto newState = m_stateFactory.find(l_type); // cambiar auto por StateFactory::iterator
	if (newState == m_stateFactory.end())
		return; // si no está en el mapa no se puede crear y salimos
		// Build State Here --> call new StateManager 
	BaseState* state = newState->second();
	// inicializamos la vista al crear el estado con la por defecto
	state->m_view = m_shared->m_wind->GetRenderWindow()->getDefaultView();
	m_states.emplace_back(l_type, state);
	state->OnCreate();
}

void StateManager::RemoveState(const StateType& l_type)
{
	for (auto itr = m_states.begin(); itr != m_states.end(); ++itr) {
		if (itr->first == l_type){
			itr->second->OnDestroy();
			delete itr->second;
			m_states.erase(itr);
			return;
		}
	}
}
