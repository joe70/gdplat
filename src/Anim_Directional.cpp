#include "../include/Anim_Directional.hpp"
#include "../include/SpriteSheet.hpp"
#include <iostream>

void Anim_Directional::CropSprite(){
sf::IntRect rect(m_spriteSheet->GetSpriteSize().x * m_frameCurrent,
		m_spriteSheet->GetSpriteSize().y * (m_frameRow + (short)m_spriteSheet->GetDirection()),
		m_spriteSheet->GetSpriteSize().x, m_spriteSheet->GetSpriteSize().y);
	m_spriteSheet->CropSprite(rect);
	std::cout << "left:" << rect.left << " top:" << rect.top << " ancho:" << rect.width << " alto:" << rect.height << std::endl;
}

void Anim_Directional::FrameStep(){
	if (m_frameStart < m_frameEnd){ ++m_frameCurrent; }
	else { --m_frameCurrent; }

	std::cout << "frame actual:"<< m_frameCurrent << std::endl;	
	if ((m_frameStart < m_frameEnd && m_frameCurrent > m_frameEnd) ||
		(m_frameStart > m_frameEnd && m_frameCurrent < m_frameEnd))
	{
		if (m_loop) { 
			m_frameCurrent = m_frameStart; 
			std::cout << "dentro loop - frame actual " << m_frameCurrent << std::endl;
			return; 
		}
		m_frameCurrent = m_frameEnd;
		Pause();
	}
}

void Anim_Directional::ReadIn(std::stringstream& l_stream){
	l_stream >> m_frameStart >> m_frameEnd >> m_frameRow
		>> m_frameTime >> m_frameActionStart >> m_frameActionEnd;
}
