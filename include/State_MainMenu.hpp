#ifndef __STATE_MAINMENU_HPP__
#define __STATE_MAINMENU_HPP__

#include "BaseState.hpp"
#include "EventManager.hpp"

class State_MainMenu : public BaseState
{
private:
    sf::Font m_font;
	sf::Text m_text;

	sf::Vector2f m_buttonSize;
	sf::Vector2f m_buttonPos;
	unsigned int m_buttonPadding;

	sf::RectangleShape m_rects[3];
	sf::Text m_labels[3];
public:
    State_MainMenu(StateManager* l_stateManager);
    virtual ~State_MainMenu();

    virtual void OnCreate();
	virtual void OnDestroy();

	virtual void Activate();
	virtual void Deactivate();

	virtual void Update(const sf::Time& l_time);
	virtual void Draw();

	void MouseClick(EventDetails* l_details);
};

#endif // __STATE_MAINMENU_HPP__