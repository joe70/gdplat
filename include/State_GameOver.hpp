#ifndef __STATE_GAMEOVER_HPP__
#define __STATE_GAMEOVER_HPP__

#include "BaseState.hpp"
#include <SFML/Graphics.hpp>

class State_GameOver : public BaseState 
{
    private:
        sf::Font m_font;
        sf::Text m_text;
        float m_elapsed;
    public:
        State_GameOver(StateManager* l_stateManager); 
        virtual ~State_GameOver(); 

        void OnCreate();
        void OnDestroy();

        void Activate();
        void Deactivate();

        void Update(const sf::Time& l_time);
        void Draw();
};

#endif // __STATE_GAMEOVER_HPP__