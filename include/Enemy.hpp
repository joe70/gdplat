#ifndef __ENEMY_HPP__
#define __ENEMY_HPP__

#include "Character.hpp"

class Enemy : public Character 
{
private:
    sf::Vector2f m_destination;
    bool m_hasDestination;
public:
    Enemy (EntityManager* l_entityMgr);
    virtual ~Enemy ();

    void OnEntityCollision(EntityBase* l_collider, bool l_attack);
	void Update(float l_dT);
};

#endif // __ENEMY_HPP__