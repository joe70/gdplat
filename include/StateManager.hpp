#ifndef __STATEMANAGER_HPP__
#define __STATEMANAGER_HPP__

#include <vector>
#include <unordered_map>
#include <functional>

#include "SharedContext.hpp"
#include "State_Intro.hpp"
#include "State_MainMenu.hpp"
#include "State_Game.hpp"
#include "State_Paused.hpp"
#include "State_GameOver.hpp"

// TODO Credits State
enum class StateType{ Intro = 1, MainMenu, Game, Paused, GameOver, Credits };

// TODO change to smart pointer - shared ptr, no new neither delete
// State container. Mejor VECTOR, mantiene un orden similar a una STACK
using StateContainer = std::vector<std::pair<StateType, BaseState*>>;
// Type container.
using TypeContainer = std::vector<StateType>;
// State factory.
using StateFactory = std::unordered_map<StateType, std::function<BaseState*(void)>>;

class StateManager{
public:
	StateManager(SharedContext* l_shared); // puntero al contexto creado en GameClass
	~StateManager();

	void Update(const sf::Time& l_time); // Operada desde GameClass
	void Draw(); // Operada desde GameClass

	void ProcessRequests(); // hace seguimiento de tipos a eliminar y lo hace cuando no se usan al final de game loop

	SharedContext* GetContext(); // Helper method obtain context
	bool HasState(const StateType& l_type); // Helper method si un estado dado está en la pila

	void SwitchTo(const StateType& l_type); // cambia a un estado dado
	void Remove(const StateType& l_type); // quita un estado de la pila
private:
	// Methods.
	void CreateState(const StateType& l_type);
	void RemoveState(const StateType& l_type);

	template<class T>
	void RegisterState(const StateType& l_type){
		m_stateFactory[l_type] = [this]() -> BaseState*
		{
			// TODO change new use make_share ptr
			return new T(this);
		};
	}

	// Members.
	SharedContext* m_shared;
	StateContainer m_states;
	TypeContainer m_toRemove; // mantiene estados a eliminar
	StateFactory m_stateFactory; // mapea state type y la lambda que los crea
};

#endif // __STATEMANAGER_HPP__