#ifndef INCLUDE_BASESTATE_HPP_
#define INCLUDE_BASESTATE_HPP_ 

#include <SFML/Graphics.hpp>

class StateManager;

class BaseState {
    friend class StateManager;
protected: // visibles para clases amigas y derivadas
    StateManager* m_stateMgr;
	bool m_transparent; // un estado anterior debe ser dibujado
	bool m_transcendent; // un estado anterior deber ser actualizado
	sf::View m_view;


public:
    BaseState(StateManager* l_stateManager):m_stateMgr(l_stateManager),
		m_transparent(false), m_transcendent(false){}
	virtual ~BaseState(){}

	virtual void OnCreate() = 0; // state created and pushed on the stack
	virtual void OnDestroy() = 0; // state removed from the stack

	virtual void Activate() = 0; // called when the state is moved to the top of the stack
	virtual void Deactivate() = 0; // called when the estate is  removed from the top position

	virtual void Update(const sf::Time& l_time) = 0; // update state
	virtual void Draw() = 0; // draw state content
    void SetTransparent(const bool& l_transparent) { m_transparent = l_transparent; }
	bool IsTransparent()const { return m_transparent; }
	void SetTranscendent(const bool& l_transcendent){ m_transcendent = l_transcendent; }
	bool IsTranscendent()const { return m_transcendent; }
	StateManager* GetStateManager() { return m_stateMgr; }
	sf::View& GetView(){ return m_view; }
};

#endif // !INCLUDE_BASESTATE_HPP 