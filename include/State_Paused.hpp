#ifndef __STATE_PAUSED_HPP__
#define __STATE_PAUSED_HPP__

#include "BaseState.hpp"
#include "EventManager.hpp"

class State_Paused : public BaseState
{
private:
    sf::Font m_font;
	sf::Text m_text;
	sf::RectangleShape m_rect;
public:
    State_Paused(StateManager* l_stateManager);
    virtual ~State_Paused();

    virtual void OnCreate();
	virtual void OnDestroy();

	virtual void Activate();
	virtual void Deactivate();

	virtual void Update(const sf::Time& l_time);
	virtual void Draw();

	void Unpause(EventDetails* l_details);
};
#endif // __STATE_PAUSED_HPP__

