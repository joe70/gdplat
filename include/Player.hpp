#ifndef __PLAYER_HPP__
#define __PLAYER_HPP__

#include "Character.hpp"
#include "EventManager.hpp"

class Player : public Character {
public:
    Player(EntityManager* l_entityMgr);
    virtual ~Player();

    void OnEntityCollision(EntityBase* l_collider, bool l_attack);
    void React(EventDetails* l_details);
};


#endif // __PLAYER_H__