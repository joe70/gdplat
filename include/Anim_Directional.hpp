#ifndef __ANIM_DIRECTIONAL_HPP__
#define __ANIM_DIRECTIONAL_HPP__

#include "Anim_Base.hpp"
#include "Directions.hpp"

class Anim_Directional : public Anim_Base{
protected:
	void FrameStep();
	void CropSprite();
	void ReadIn(std::stringstream& l_stream);
};

#endif // __ANIM_DIRECTIONAL_HPP_