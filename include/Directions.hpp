#ifndef __DIRECTIONS_HPP__
#define __DIRECTIONS_HPP__

enum class Direction { Right=0, Left=1 };

#endif // __DIRECTIONS_HPP__