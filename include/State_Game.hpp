#ifndef __STATE_GAME_HPP__
#define __STATE_GAME_HPP__

#include "BaseState.hpp"
#include "EventManager.hpp"
#include "Map.hpp"

class State_Game : public BaseState
{
private:
	/*
    sf::Texture m_texture;
	sf::Sprite m_sprite;
	sf::Vector2f m_increment;
	*/
	Map* m_gameMap;
public:
    State_Game(StateManager* l_stateManager);
    virtual ~State_Game();

    virtual void OnCreate();
	virtual void OnDestroy();

	virtual void Activate();
	virtual void Deactivate();

	virtual void Update(const sf::Time& l_time);
	virtual void Draw();

	void MainMenu(EventDetails* l_details);
	void Pause(EventDetails* l_details);
	void ToggleOverlay(EventDetails* l_details);
};

#endif // __STATE_GAME_HPP__