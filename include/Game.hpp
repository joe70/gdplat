#ifndef INCLUDE_GAME_HPP
#define INCLUDE_GAME_HPP

#include "Window.hpp"
//#include "EventManager.hpp"
#include <iostream>
#include "StateManager.hpp"
#include "EntityManager.hpp"
#include "TextureManager.hpp"

class Game {
private:
	/*
	sf::Texture m_texture;
	sf::Sprite m_sprite;
	*/
	void RestartClock();

	sf::Clock m_clock;
	sf::Time m_elapsed;
	SharedContext m_context;
    Window m_window;
	EntityManager m_entityManager;
	TextureManager m_textureManager;
	StateManager m_stateManager;


public:
    Game();
    ~Game();

    void Update();
	void Render();
	void LateUpdate();

	sf::Time GetElapsed();
	Window* GetWindow();

	/*
	void MoveSprite(EventDetails* l_details);
	void Lurch (EventDetails* ldetails);
	void Swap (EventDetails* ldetails);
	void ComboXY (EventDetails* ldetails);
	*/
};
#endif // INCLUDE_GAME_HPP


