#ifndef __STATE_INTRO_HPP__
#define __STATE_INTRO_HPP__

#include "BaseState.hpp"
#include "EventManager.hpp"

class State_Intro : public BaseState
{
private:
    // sf::Texture m_introTexture;
	sf::Sprite m_introSprite;
	sf::Font m_font;
	sf::Text m_text;

	// float m_timePassed;
public:
    State_Intro(StateManager* l_stateManager);
    virtual ~State_Intro();

    virtual void OnCreate();
	virtual void OnDestroy();

	virtual void Activate();
	virtual void Deactivate();

	virtual void Update(const sf::Time& l_time);
	virtual void Draw();

	void Continue(EventDetails* l_details);
};

#endif // __STATE_INTRO_HPP__