#ifndef __TEXTUREMANAGER_HPP__
#define __TEXTUREMANAGER_HPP__

#include "ResourceManager.hpp"
#include <SFML/Graphics/Texture.hpp>

class TextureManager : public ResourceManager <TextureManager, sf::Texture>
{

public:
    TextureManager() : ResourceManager("textures.cfg") {}
    virtual ~TextureManager() {};

    sf::Texture* Load(const std::string& l_path) {
        sf:: Texture* texture = new sf::Texture();
        if (!texture->loadFromFile(Utils::GetWorkingDirectory()+l_path)) {
            delete texture;
            texture = nullptr;
            std::cerr << "!Failed to load texture: " << l_path << std::endl;
        }
        return texture;
    }
};

#endif // __TEXTUREMANAGER_HPP__