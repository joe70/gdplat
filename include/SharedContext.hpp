#ifndef __SHAREDCONTEXT_H__
#define __SHAREDCONTEXT_H__

#include "Window.hpp"
#include "EventManager.hpp"
#include "TextureManager.hpp"
#include "EntityManager.hpp"
#include "DebugOverlay.hpp"

class Map;
struct SharedContext{
	Window* m_wind;
	EventManager* m_eventManager;
	TextureManager* m_textureManager;
	EntityManager* m_entityManager;
	Map* m_gameMap;
	DebugOverlay m_debugOverlay;
	
	SharedContext() : m_wind(nullptr), m_eventManager(nullptr),
	m_textureManager(nullptr), m_entityManager(nullptr),
	m_gameMap(nullptr) {}
	
};

#endif // __SHAREDCONTEXinclude/SharedContext.hppT_H__